<?php
namespace app\admin\controller;
use think\Controller;
use think\Loader;
use catetree\Catetree;
use app\admin\validate\TypeValidate;
use app\admin\model\GoodsModel;
/**
 * 商品管理
 */
class Goods extends Controller
{
	//列表页
	public function lists(){
		$join = [
            ['category c','g.category_id=c.id'],
            ['brand b','g.brand_id=b.id','LEFT'],
            ['type t','g.type_id=t.id','LEFT'],
            ['product p','g.id=p.goods_id','LEFT'],
        ];
    	$goodsRes=db('goods')->alias('g')->field('g.*,c.cate_name,b.brand_name,t.type_name,SUM(p.goods_number) gn')->join($join)->group('g.id')->order('g.id DESC')->paginate(6);

		$this->assign(array(
			'goodsRes'=>$goodsRes,
		));
		return view();
	}
	// 添加
	public function add(){
		if (request()->isPost()) {
			$data=input('post.');
			//验证
			// $validate =new TypeValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			$add=model('GoodsModel')->save($data);
			if($add){
				$this->success('商品添加成功','lists');
			}else{
				$this->error('商品添加失败');
			}
			return;
		}
		 // 会员级别数据
        $mlRes=db('memberLevel')->field('id,level_name')->select();
        // 获取类型
        $typeRes=db('type')->select();
        // 品牌分类
        $brandRes=db('brand')->field('id,brand_name')->select();
        //商品推荐位
        $goodsRecposRes=db('recpos')->where('rec_type','=',1)->select();
        // 商品分类
        $Category=new Catetree();
        $CategoryObj=db('Category');
        $CategoryRes=$CategoryObj->order('sort DESC')->select();
        $CategoryRes=$Category->Catetree($CategoryRes);
        $this->assign([
            'mlRes'=>$mlRes,
            'typeRes'=>$typeRes,
            'brandRes'=>$brandRes,
            'CategoryRes'=>$CategoryRes,
            'goodsRecposRes'=>$goodsRecposRes,
            ]);
		return view();
	}
	// 编辑
	public function edit(){
		if (request()->isPost()) {
			$data=input('post.');
			// dump($data);exit;
			//验证
			// $validate =new TypeValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			$save=model('GoodsModel')->update($data);
			if($save!==false){
				$this->success('商品编辑成功','lists');
			}else{
				$this->error('商品编辑失败');
			}
			return;
		}
		// 商品id
		$goodsId=input('id');
		// 查询当前商品信息
		$goods=db('goods')->where(array('id'=>$goodsId))->find();
		// 会员价格
		$memberPrice=db('member_price')->where('goods_id','=',$goodsId)->select();
		$mpRes=array();
		foreach ($memberPrice as $k => $v) {
			$mpRes[$v['mlevel_id']]=$v;
		}
		// 商品相册
        $gphotoRes=db('goods_photo')->where('goods_id','=',$goodsId)->select();
		 // 会员级别数据
        $mlRes=db('memberLevel')->field('id,level_name')->select();
        // 获取类型
        $typeRes=db('type')->select();
       // 查询当前商品类型所有属性信息
        $attrRes=db('attr')->where('type_id','=',$goods['type_id'])->select();
        // 查询当前商品拥有的商品属性goods_attr
        $_gattrRes=db('goods_attr')->where('goods_id','=',$goodsId)->select();
        $gattrRes=array();
        foreach ($_gattrRes as $k => $v) {
        	$gattrRes[$v['attr_id']][]=$v;
        }
        // dump($gattrRes);
        // dump($attrRes);exit;
        // 品牌分类
        $brandRes=db('brand')->field('id,brand_name')->select();
        //商品推荐位
        $goodsRecposRes=db('recpos')->where('rec_type','=',1)->select();
        //当前商品推荐位
        $_goodsItem=db('item')->where(array('value_id'=>$goodsId,'value_type'=>1))->select();
        $goodsItem=array();
        foreach ($_goodsItem as $k => $v) {
        	$goodsItem[]=$v['recpos_id'];
        }
        // 商品分类
        $Category=new Catetree();
        $CategoryObj=db('Category');
        $CategoryRes=$CategoryObj->order('sort DESC')->select();
        $CategoryRes=$Category->Catetree($CategoryRes);
		$this->assign([
			'goods'=>$goods,
			'mlRes'=>$mlRes,
            'typeRes'=>$typeRes,
            'brandRes'=>$brandRes,
            'CategoryRes'=>$CategoryRes,
            'goodsRecposRes'=>$goodsRecposRes,
            'memberPrice'=>$memberPrice,
            'mpRes'=>$mpRes,
            'gphotoRes'=>$gphotoRes,
            'attrRes'=>$attrRes,
            'gattrRes'=>$gattrRes,
            'goodsItem'=>$goodsItem,
			]);
		return view();
	}
	// 删除
	public function del($id){
		$del=model('GoodsModel')->destroy($id);
		if($del){
				$this->success('商品删除成功','lists');
			}else{
				$this->error('商品删除失败');
		}
	}
	// 图片上传处理
	public  function  upload(){
	 // 获取表单上传文件 例如上传了001.jpg
	    $file = request()->file('type_logo');
	    // 移动到框架应用根目录/public/uploads/ 目录下
	    if($file){
	        $info = $file->move(ROOT_PATH . 'public' . DS .'static'.DS. 'uploads');
	        if($info){
	            return $info->getSaveName();
	        }else{
	            // 上传失败获取错误信息
	            echo $file->getError();exit;
	        }
	    }
	}

	// 商品库存添加
	public  function  product($id){
		if (Request()->isPost()) {
			db('product')->where(array('goods_id'=>$id))->delete();
			$data=input('post.');
			$goodsAttr=$data['goods_attr'];
			$goodsNum=$data['goods_num'];
			$product=db('product');
			foreach ($goodsNum as $k => $v) {
				 $strArr=array();
                foreach ($goodsAttr as $k1 => $v1) {
                    if(intval($v1[$k])<=0){
                        continue 2;
                    }
                    $strArr[]=$v1[$k];
                }
                sort($strArr);
                $strArr=implode(',', $strArr);
                $product->insert([
                    'goods_id'=>$id,
                    'goods_number'=>$v,
                    'goods_attr'=>$strArr
                    ]);
			}
			  $this->success('添加库存成功！');
            return;

		}
		$_radioAttrRes=db('goods_attr')->alias('g')->field('g.id,g.attr_id,g.attr_value,a.attr_name')->join('attr a','a.id=g.attr_id')->where(array('g.goods_id'=>$id,'a.attr_type'=>1))->select();
		$radioAttrRes=array();
		// 格式化数组
		foreach ($_radioAttrRes as $k => $v) {
			$radioAttrRes[$v['attr_name']][]=$v;
		}
		// dump($_radioAttrRes);
		// dump($radioAttrRes);exit;
		// 商品库存信息
		$goodsProRes=db('product')->where(array('goods_id'=>$id))->select();
		$this->assign([
			'radioAttrRes'=>$radioAttrRes,
			'goodsProRes'=>$goodsProRes,
			]);
		return view();
	}
	//删除商品图册
	public function ajaxdelpic(){
		$id=input('id');
		$photo=db('goods_photo')->find($id);
		$og_photo=IMG_UPLOADS.$photo['og_photo'];
		$big_photo=IMG_UPLOADS.$photo['big_photo'];
		$mid_photo=IMG_UPLOADS.$photo['mid_photo'];
		$sm_photo=IMG_UPLOADS.$photo['sm_photo'];
		@unlink($og_photo);
		@unlink($big_photo);
		@unlink($mid_photo);
		@unlink($sm_photo);
		$del=db('goods_photo')->where('id',$id)->delete();
		if ($del) {
			echo 1;exit;
		}else{
			echo 2;exit;
		}
	}
}
