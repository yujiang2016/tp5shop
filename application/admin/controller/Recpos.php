<?php
namespace app\admin\controller;
use think\Controller;
use think\Loader;
use app\admin\validate\recposValidate;
/**
 * 推荐位管理
 */
class Recpos extends Controller
{
	//列表页
	public function lists(){
		$data=db('recpos')->order('id desc')->paginate(10);
		$this->assign('data',$data);
		return view();
	}
	// 添加
	public function add(){
		if (request()->isPost()) {
			$data=input('post.');
		
			//验证
			// $validate =new recposValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			$add=db('recpos')->insert($data);
			if($add){
				$this->success('推荐位添加成功','recpos/lists');
			}else{
				$this->error('推荐位添加失败');
			}
			return;
		}
		return view();
	}
	// 编辑
	public function edit(){
		if (request()->isPost()) {
			$data=input('post.');
			
			//验证
			// $validate =new recposValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			$save=db('recpos')->update($data);
			if($save!==false){
				$this->success('推荐位编辑成功','recpos/lists');
			}else{
				$this->error('推荐位编辑失败');
			}
			return;
		}
		$recpos=db('recpos')->where('id',input('id'))->find();
		$this->assign('recpos',$recpos);
		return view();
	}
	// 删除
	public function del($id){
		$del=db('recpos')->delete($id);
		if($del){
				$this->success('推荐位删除成功','recpos/lists');
			}else{
				$this->error('推荐位删除失败');
		}
	}
	// 图片上传处理
	public  function  upload(){
	 // 获取表单上传文件 例如上传了001.jpg
    $file = request()->file('recpos_logo');
    // 移动到框架应用根目录/public/uploads/ 目录下
    if($file){
        $info = $file->move(ROOT_PATH . 'public' . DS .'static'.DS. 'uploads');
        if($info){
            return $info->getSaveName();
        }else{
            // 上传失败获取错误信息
            echo $file->getError();exit;
        }
    }
	}
}
