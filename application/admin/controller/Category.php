<?php
namespace app\admin\controller;
use think\Controller;
use catetree\Catetree;
use app\admin\validate\CategoryValidate;
/**
 * 商品分类
 */
class Category extends Controller
{
	//列表页
	public function lists(){
		$categoryObj=db('category');
		if (request()->isPost()) {
			$data=input('post.');
			foreach ($data['sort'] as $k => $v) {
				$save=$categoryObj->where(array('id'=>$k))->update(['sort'=>$v]);
			}
			$this->success('排序修改成功');
		}
		$category=new Catetree();
		$categoryRes=$categoryObj->order('sort desc')->select();
		$categoryRes=$category->catetree($categoryRes);
		$this->assign('categoryRes',$categoryRes);
		return view();
	}
	// 添加
	public function add(){
		$categoryObj=db('category');
		$categoryRes=new Catetree();
		if (request()->isPost()) {
			$data=input('post.');
			//验证
			// $validate =new categoryValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			// 处理上传图片
			if ($_FILES['cate_img']['tmp_name']) {
				$data['cate_img']=$this->upload();
			}
			$add=model('CategoryModel')->save($data);
			if($add){
				$this->success('分类添加成功','category/lists');
			}else{
				$this->error('分类添加失败');
			}
			return;
		}
		//商品推荐位
        $goodsRecposRes=db('recpos')->where('rec_type','=',2)->select();
		$category=$categoryObj->order('sort desc')->select();
		$category=$categoryRes->catetree($category);
		$this->assign([
			'category'=>$category,
			'goodsRecposRes'=>$goodsRecposRes,
			]);
		return view();
	}
	// 编辑
	public function edit(){
		$categoryObj=db('category');
		$categoryRes=new Catetree();
		if (request()->isPost()) {
			$data=input('post.');
			//验证
			// $validate =new CategoryValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			 //处理图片上传
            if($_FILES['cate_img']['tmp_name']){
                $data['cate_img']=$this->upload();
                $categorys=$categoryObj->field('cate_img')->find($data['id']);
                if($categorys['cate_img']){
                    $imgSrc=IMG_UPLOADS.$categorys['cate_img'];
                    if(file_exists($imgSrc)){
                        @unlink($imgSrc);
                    }
                }
            }
			$save=model('CategoryModel')->update($data);
			if($save!==false){
				$this->success('分类编辑成功','category/lists');
			}else{
				$this->error('分类编辑失败');
			}
			return;
		}
		$categoryId=input('id');
		//商品推荐位
        $goodsRecposRes=db('recpos')->where('rec_type','=',2)->select();
        //当前商品推荐位
        $_goodsItem=db('item')->where(array('value_id'=>$categoryId,'value_type'=>2))->select();
        $goodsItem=array();
        foreach ($_goodsItem as $k => $v) {
        	$goodsItem[]=$v['recpos_id'];
        }
		$category=$categoryObj->order('sort desc')->select();
		$category=$categoryRes->catetree($category);
		$gory=db('category')->where('id',$categoryId)->find();
		$this->assign(array(
			'category'=>$category,
			'gory'=>$gory,
			'goodsRecposRes'=>$goodsRecposRes,
			'goodsItem'=>$goodsItem,
		));
		return view();
	}
	// 删除
	public function del($id){
		$categoryTree=new Catetree();
		$category=db('category');
		$sonids=$categoryTree->childrenids($id,$category);
		$sonids[]=intval($id);//当前栏目id
		// halt($sonids);
		// 删除之前判断分类文章及相关缩略图的删除
		foreach ($sonids as $k => $v) {
			$artRes=$category->field('id,cate_img')->where(array('id'=>$v))->select();
			foreach ($artRes as $k1 => $v1) {
				$picSrc=IMG_UPLOADS.$v1['cate_img'];
				if (file_exists($picSrc)) {
					@unlink($picSrc);
				}
			}
		}
		//删除栏目前，检查并删除当前栏目的推荐信息
        $recItem=db('item');
        foreach ($sonids as $k => $v) {
            $recItem->where(array('value_id'=>$v,'value_type'=>2))->delete();
        }
		$del=$category->delete($sonids);
		if($del){
				$this->success('分类删除成功','lists');
			}else{
				$this->error('分类删除失败');
		}
	}
		// 图片上传处理
	public  function  upload(){
	 // 获取表单上传文件 例如上传了001.jpg
    $file = request()->file('cate_img');
    // 移动到框架应用根目录/public/uploads/ 目录下
	    if($file){
	        $info = $file->move(ROOT_PATH . 'public' . DS .'static'.DS. 'uploads');
	        if($info){
	            return $info->getSaveName();
	        }else{
	            // 上传失败获取错误信息
	            echo $file->getError();exit;
	        }
	    }
	}
	
}
