<?php
namespace app\admin\controller;
use think\Controller;
use think\Loader;
use app\admin\validate\AttrValidate;
/**
 * 商品属性管理
 */
class Attr extends Controller
{
	//列表页
	public function lists(){
		 $typeId=input('type_id');
        if($typeId){
            $map['type_id']=['=',$typeId];
        }else{
            $map=1;
        }
		$data=db('attr')->where($map)
		->field('a.id,a.attr_name,a.attr_values,a.attr_type,t.type_name')
		->alias('a')->join('type t','t.id=a.type_id')->order('id desc')->paginate(10);
		$this->assign('data',$data);
		return view();
	}
	// 添加
	public function add(){
		if (request()->isPost()) {
			$data=input('post.');
		
			//验证
			// $validate =new AttrValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			$data['attr_values']=str_replace('，', ',', $data['attr_values']);
			$add=db('attr')->insert($data);
			if($add){
				$this->success('属性添加成功','attr/lists');
			}else{
				$this->error('属性添加失败');
			}
			return;
		}
		$type=db('type')->select();
		$this->assign('type',$type);
		return view();
	}
	// 编辑
	public function edit(){
		if (request()->isPost()) {
			$data=input('post.');
			
			//验证
			// $validate =new AttrValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			$data['attr_values']=str_replace('，', ',', $data['attr_values']);
			$save=db('attr')->update($data);
			if($save!==false){
				$this->success('属性编辑成功','attr/lists');
			}else{
				$this->error('属性编辑失败');
			}
			return;
		}
		$type=db('type')->select();
		$attr=db('attr')->where('id',input('id'))->find();
		$this->assign(array('type'=>$type,'attr'=>$attr));
		return view();
	}
	// 删除
	public function del($id){
		$del=db('attr')->delete($id);
		if($del){
				$this->success('属性删除成功','attr/lists');
			}else{
				$this->error('属性删除失败');
		}
	}
	// 图片上传处理
	public  function  upload(){
	 // 获取表单上传文件 例如上传了001.jpg
    $file = request()->file('attr_logo');
    // 移动到框架应用根目录/public/uploads/ 目录下
    if($file){
        $info = $file->move(ROOT_PATH . 'public' . DS .'static'.DS. 'uploads');
        if($info){
            return $info->getSaveName();
        }else{
            // 上传失败获取错误信息
            echo $file->getError();exit;
        }
    }
	}
	// 异步获取指定属性下的属性
    public function ajaxGetAttr(){
        $typeId=input('type_id');
        $attrRes=db('attr')->where(array('type_id'=>$typeId))->select();
        echo json_encode($attrRes);
    }
}
