<?php
namespace app\admin\controller;
use think\Controller;
use think\Loader;
use app\admin\validate\TypeValidate;
/**
 * 会员级别管理
 */
class MemberLevel extends Controller
{
	//列表页
	public function lists(){
		$data=db('member_level')->order('id desc')->paginate(10);
		$this->assign('data',$data);
		return view();
	}
	// 添加
	public function add(){
		if (request()->isPost()) {
			$data=input('post.');
		
			//验证
			// $validate =new TypeValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			$add=db('member_level')->insert($data);
			if($add){
				$this->success('会员级别添加成功','lists');
			}else{
				$this->error('会员级别添加失败');
			}
			return;
		}
		return view();
	}
	// 编辑
	public function edit(){
		if (request()->isPost()) {
			$data=input('post.');
			
			//验证
			// $validate =new TypeValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			$save=db('member_level')->update($data);
			if($save!==false){
				$this->success('会员级别编辑成功','lists');
			}else{
				$this->error('会员级别编辑失败');
			}
			return;
		}
		$level=db('member_level')->where('id',input('id'))->find();
		$this->assign('level',$level);
		return view();
	}
	// 删除
	public function del($id){
		$del=db('member_level')->delete($id);
		if($del){
				$this->success('会员级别删除成功','lists');
			}else{
				$this->error('会员级别删除失败');
		}
	}
	
}
