<?php
namespace app\admin\controller;
use think\Controller;
use think\Loader;
use app\admin\validate\LinkValidate;
/**
 * 链接管理
 */
class Link extends Controller
{
	//列表页
	public function lists(){
		$link=db('link')->order('id desc')->paginate(10);
		$this->assign('link',$link);
		return view();
	}
	// 添加
	public function add(){
		if (request()->isPost()) {
			$data=input('post.');
			if($data['link_url'] && stripos($data['link_url'], 'http://')===false){
				$data['link_url']='http://'.$data['link_url'];
			}
			// 处理上传图片
			if ($_FILES['logo']['tmp_name']) {
				$data['logo']=$this->upload();
			}
			//验证
			$validate =new LinkValidate();
			if(!$validate->check($data)){
    			$this->error($validate->getError());
			}
			$add=db('link')->insert($data);
			if($add){
				$this->success('链接添加成功','link/lists');
			}else{
				$this->error('链接添加失败');
			}
			return;
		}
		return view();
	}
	// 编辑
	public function edit(){
		if (request()->isPost()) {
			$data=input('post.');
			if($data['link_url'] && stripos($data['link_url'], 'http://')===false){
				$data['link_url']='http://'.$data['link_url'];
			}
			// 处理上传图片
			if ($_FILES['logo']['tmp_name']) {
				$oldImg=db('link')->field('logo')->find($data['id']);
				$imgPic=IMG_UPLOADS.$oldImg['logo'];
				if (file_exists($imgPic)) {
					@unlink($imgPic);
				}
				$data['logo']=$this->upload();
			}
			//验证
			$validate =new LinkValidate();
			if(!$validate->check($data)){
    			$this->error($validate->getError());
			}
			$save=db('link')->update($data);
			if($save!==false){
				$this->success('链接编辑成功','link/lists');
			}else{
				$this->error('链接编辑失败');
			}
			return;
		}
		$link=db('link')->where('id',input('id'))->find();
		$this->assign('link',$link);
		return view();
	}
	// 删除
	public function del($id){
		
		$oldImg=db('link')->field('logo')->find($id);
		$imgPic=IMG_UPLOADS.$oldImg['logo'];
		if (file_exists($imgPic)) {
					@unlink($imgPic);
		}
		$del=db('link')->delete($id);
		if($del){
				$this->success('链接删除成功','link/lists');
			}else{
				$this->error('链接删除失败');
		}
	}
	// 图片上传处理
	public  function  upload(){
	 // 获取表单上传文件 例如上传了001.jpg
    $file = request()->file('logo');
    // 移动到框架应用根目录/public/uploads/ 目录下
    if($file){
        $info = $file->move(ROOT_PATH . 'public' . DS .'static'.DS. 'uploads');
        if($info){
            return $info->getSaveName();
        }else{
            // 上传失败获取错误信息
            echo $file->getError();exit;
        }
    }
	}
}
