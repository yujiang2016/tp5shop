<?php
namespace app\admin\controller;
use think\Controller;
use think\Loader;
use app\admin\validate\TypeValidate;
/**
 * 商品类型管理
 */
class Type extends Controller
{
	//列表页
	public function lists(){
		$data=db('type')->order('id desc')->paginate(10);
		$this->assign('data',$data);
		return view();
	}
	// 添加
	public function add(){
		if (request()->isPost()) {
			$data=input('post.');
		
			//验证
			$validate =new TypeValidate();
			if(!$validate->check($data)){
    			$this->error($validate->getError());
			}
			$add=db('type')->insert($data);
			if($add){
				$this->success('商品类型添加成功','type/lists');
			}else{
				$this->error('商品类型添加失败');
			}
			return;
		}
		return view();
	}
	// 编辑
	public function edit(){
		if (request()->isPost()) {
			$data=input('post.');
			
			//验证
			$validate =new TypeValidate();
			if(!$validate->check($data)){
    			$this->error($validate->getError());
			}
			$save=db('type')->update($data);
			if($save!==false){
				$this->success('商品类型编辑成功','type/lists');
			}else{
				$this->error('商品类型编辑失败');
			}
			return;
		}
		$type=db('type')->where('id',input('id'))->find();
		$this->assign('type',$type);
		return view();
	}
	// 删除
	public function del($id){
		$del=db('type')->delete($id);
		db('attr')->where(array('type_id'=>$id))->delete();//删除类型下的属性
		if($del){
				$this->success('商品类型删除成功','type/lists');
			}else{
				$this->error('商品类型删除失败');
		}
	}
	// 图片上传处理
	public  function  upload(){
	 // 获取表单上传文件 例如上传了001.jpg
    $file = request()->file('type_logo');
    // 移动到框架应用根目录/public/uploads/ 目录下
    if($file){
        $info = $file->move(ROOT_PATH . 'public' . DS .'static'.DS. 'uploads');
        if($info){
            return $info->getSaveName();
        }else{
            // 上传失败获取错误信息
            echo $file->getError();exit;
        }
    }
	}
}
