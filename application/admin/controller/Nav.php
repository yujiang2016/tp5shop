<?php
namespace app\admin\controller;
use think\Controller;
use catetree\Catetree;
/**
 * 导航
 */
class Nav extends Controller
{
	//列表页
	public function lists(){
			$nav=db('nav');
		if (request()->isPost()) {
			$data=input('post.');
			foreach ($data['sort'] as $k => $v) {
				$nav->where('id','=',$k)->update(['sort'=>$v]);
			}
			$this->success('排序成功','lists');
			
		}
		$data=$nav->order('sort DESC')->paginate(10);
		$this->assign('data',$data);
		return view();
	}
	// 添加
	public function add(){
		if (request()->isPost()) {
			$data=input('post.');
			if($data['nav_url'] && stripos($data['nav_url'], 'http://')===false){
				$data['nav_url']='http://'.$data['nav_url'];
			}
			//验证
			// $validate =new navValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			$add=db('nav')->insert($data);
			if($add){
				$this->success('导航添加成功','nav/lists');
			}else{
				$this->error('导航添加失败');
			}
			return;
		}
		return view();
	}
	// 编辑
	public function edit(){
		if (request()->isPost()) {
			$data=input('post.');
			if($data['nav_url'] && stripos($data['nav_url'], 'http://')===false){
				$data['nav_url']='http://'.$data['nav_url'];
			}
		
			//验证
			// $validate =new navValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			$save=db('nav')->update($data);
			if($save!==false){
				$this->success('导航编辑成功','nav/lists');
			}else{
				$this->error('导航编辑失败');
			}
			return;
		}
		$nav=db('nav')->where('id',input('id'))->find();
		$this->assign('nav',$nav);
		return view();
	}
	// 删除
	public function del($id){
		$del=db('nav')->delete($id);
		if($del){
				$this->success('导航删除成功','nav/lists');
			}else{
				$this->error('导航删除失败');
		}
	}

}
