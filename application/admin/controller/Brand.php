<?php
namespace app\admin\controller;
use think\Controller;
use think\Loader;
use app\admin\validate\BrandValidate;
/**
 * 商品管理
 */
class Brand extends Controller
{
	//列表页
	public function lists(){
		$data=db('brand')->order('id desc')->paginate(10);
		$this->assign('data',$data);
		return view();
	}
	// 添加
	public function add(){
		if (request()->isPost()) {
			$data=input('post.');
			if($data['brand_url'] && stripos($data['brand_url'], 'http://')===false){
				$data['brand_url']='http://'.$data['brand_url'];
			}
			// 处理上传图片
			if ($_FILES['brand_logo']['tmp_name']) {
				$data['brand_logo']=$this->upload();
			}
			//验证
			$validate =new BrandValidate();
			if(!$validate->check($data)){
    			$this->error($validate->getError());
			}
			$add=db('brand')->insert($data);
			if($add){
				$this->success('品牌添加成功','brand/lists');
			}else{
				$this->error('品牌添加失败');
			}
			return;
		}
		return view();
	}
	// 编辑
	public function edit(){
		if (request()->isPost()) {
			$data=input('post.');
			if($data['brand_url'] && stripos($data['brand_url'], 'http://')===false){
				$data['brand_url']='http://'.$data['brand_url'];
			}
			// 处理上传图片
			if ($_FILES['brand_logo']['tmp_name']) {
				$oldImg=db('brand')->field('brand_logo')->find($data['id']);
				$imgPic=IMG_UPLOADS.$oldImg['brand_logo'];
				if (file_exists($imgPic)) {
					@unlink($imgPic);
				}
				$data['brand_logo']=$this->upload();
			}
			//验证
			$validate =new BrandValidate();
			if(!$validate->check($data)){
    			$this->error($validate->getError());
			}
			$save=db('brand')->update($data);
			if($save!==false){
				$this->success('品牌编辑成功','brand/lists');
			}else{
				$this->error('品牌编辑失败');
			}
			return;
		}
		$brand=db('brand')->where('id',input('id'))->find();
		$this->assign('brand',$brand);
		return view();
	}
	// 删除
	public function del($id){
		$del=db('brand')->delete($id);
		if($del){
				$this->success('品牌删除成功','brand/lists');
			}else{
				$this->error('品牌删除失败');
		}
	}
	// 图片上传处理
	public  function  upload(){
	 // 获取表单上传文件 例如上传了001.jpg
    $file = request()->file('brand_logo');
    // 移动到框架应用根目录/public/uploads/ 目录下
    if($file){
        $info = $file->move(ROOT_PATH . 'public' . DS .'static'.DS. 'uploads');
        if($info){
            return $info->getSaveName();
        }else{
            // 上传失败获取错误信息
            echo $file->getError();exit;
        }
    }
	}
}
