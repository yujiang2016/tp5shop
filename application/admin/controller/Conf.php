<?php
namespace app\admin\controller;
use think\Controller;
use think\Loader;
use app\admin\validate\ConfValidate;
/**
 * 配置管理
 */
class Conf extends Controller
{
	//配置列表页
	public function lists(){
		$confRes=db('conf');
		if (request()->isPost()) {
			$conf=input('post.');
			foreach ($conf['sort'] as $k => $v) {
				$save=$confRes->update(['id'=>$k,'sort'=>$v]);
				
			}
			if ($save) {
					$this->success('排序成功','lists');
				}

		}
		$data=$confRes->order('sort desc')->paginate(10);
		$this->assign('data',$data);
		return view();
	}
	// 配置项
	public  function conflist(){
		$confRes=db('conf');
		if (request()->isPost()) {
			$data=input('post.');
			// 复选框空选问题
			$checkFields2D=$confRes->field('ename')->where(array('form_type'=>'checkbox'))->select();
			// 改为一维数组
			$checkFields=array();
			if ($checkFields2D) {
				foreach ($checkFields2D as $k => $v) {
					$checkFields[]=$v['ename'];
				}
			}
			 // 所有发送的字段组成的数组
            $allFields=array();
			// 处理文字
			foreach ($data as $k => $v) {
				$allFields[]=$k;
				if (is_array($v)) {
					$value=implode(',', $v);
					$confRes->where(array('ename'=>$k))->update(['value'=>$value]);
				}else{
					$confRes->where(array('ename'=>$k))->update(['value'=>$v]);
				}
			}
			// 如果复选框发送过来的为空
			foreach ($checkFields as $k => $v) {
				if (!in_array($v, $allFields)) {
					$confRes->where(array('ename'=>$k))->update(['value'=>'']);
				}
			}
			// 处理图片
			if ($_FILES) {
				foreach ($_FILES as $k => $v) {
					if ($v['tmp_name']) {
						$imgs=$confRes->field('value')->where(array('ename'=>$k))->find();
						if ($imgs['value']) {
							$oldImg=IMG_UPLOADS.$imgs['value'];
							if (file_exists($oldImg)) {
								@unlink($oldImg);
							}
						}
					}
					$imgSrc=$this->upload($k);
					$confRes->where(array('ename'=>$k))->update(['value'=>$imgSrc]);
				}
			}
			$this->success('配置修改成功');
		}
		$ShopConfRes=$confRes->where(array('conf_type'=>1))->order('sort desc')->select();
		$GoodsConfRes=$confRes->where(array('conf_type'=>2))->order('sort desc')->select();
		$this->assign(array(
			'ShopConfRes'=>$ShopConfRes,
			'GoodsConfRes'=>$GoodsConfRes,
		));
		return view();
	}
	// 添加
	public function add(){
		if (request()->isPost()) {
			$data=input('post.');
			// 如果是多选或单选或下拉框，替换中文，为英文dou,
			if ($data['form_type']=='radio' ||$data['form_type']=='select'||$data['form_type']=='checkbox') {
				$data['values']=str_replace('，', ',', $data['values']);
				$data['value']=str_replace('，', ',', $data['value']);
			}
			//验证
			// $validate =new ConfValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			$add=db('conf')->insert($data);
			if($add){
				$this->success('配置添加成功','conf/lists');
			}else{
				$this->error('配置添加失败');
			}
			return;
		}
		return view();
	}
	// 编辑
	public function edit(){
		if (request()->isPost()) {
			$data=input('post.');
			// 如果是多选或单选或下拉框，替换中文"，"为英文","
			if ($data['form_type']=='radio' ||$data['form_type']=='select'||$data['form_type']=='checkbox') {
				$data['values']=str_replace('，', ',', $data['values']);
				$data['value']=str_replace('，', ',', $data['value']);
			}
			//验证 
			// $validate =new ConfValidate();
			// if(!$validate->check($data)){
   //  			$this->error($validate->getError());
			// }
			$save=db('conf')->update($data);
			if($save!==false){
				$this->success('配置编辑成功','conf/lists');
			}else{
				$this->error('配置编辑失败');
			}
			return;
		}
		$conf=db('conf')->where('id',input('id'))->find();
		$this->assign('conf',$conf);
		return view();
	}
	// 删除
	public function del($id){
		$del=db('conf')->delete($id);
		if($del){
				$this->success('配置删除成功','conf/lists');
			}else{
				$this->error('配置删除失败');
		}
	}
	// 图片上传处理
	public  function  upload($imgName){
	 // 获取表单上传文件 例如上传了001.jpg
    $file = request()->file($imgName);
    // 移动到框架应用根目录/public/uploads/ 目录下
    if($file){
        $info = $file->move(ROOT_PATH . 'public' . DS .'static'.DS. 'uploads');
        if($info){
            return $info->getSaveName();
        }else{
            // 上传失败获取错误信息
            echo $file->getError();exit;
        }
    }
	}
}
