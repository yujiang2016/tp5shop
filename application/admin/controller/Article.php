<?php
namespace app\admin\controller;
use think\Controller;
use think\Loader;
use app\admin\validate\ArticleValidate;
use catetree\Catetree;
/**
 * 文章管理
 */
class Article extends Controller
{
	//列表页
	public function lists(){
		$data=db('article')->field('a.id,a.title,a.thumb,a.addtime,b.cate_name,a.show_top,a.link_url,a.show_status')->order('a.id desc')->alias('a')->join('cate b','b.id=a.cate_id')->paginate(10);
		$this->assign('data',$data);
		return view();
	}
	// 添加
	public function add(){
		if (request()->isPost()) {
			$data=input('post.');
			if($data['link_url'] && stripos($data['link_url'], 'http://')===false){
				$data['link_url']='http://'.$data['link_url'];
			}

			// 处理上传图片
			if ($_FILES['thumb']['tmp_name']) {
				$data['thumb']=$this->upload();
			} 	
			$data['addtime']=date('Y-m-d H:i:s');
			//验证
			$validate =new ArticleValidate();
			if(!$validate->check($data)){
    			$this->error($validate->getError());
			}
			$add=db('article')->insert($data);
			if($add){
				$this->success('文章添加成功','article/lists');
			}else{
				$this->error('文章添加失败');
			}
			return;
		}
		$cateObj=db('cate');
		$cateRes=new Catetree();
		$cate=$cateObj->select();
		$cate=$cateRes->catetree($cate,$cateObj);
		$this->assign('cate',$cate);
		return view();
	}
	// 编辑
	public function edit(){
		if (request()->isPost()) {
			$data=input('post.');
			if($data['link_url'] && stripos($data['link_url'], 'http://')===false){
				$data['link_url']='http://'.$data['link_url'];
			}
			// 处理上传图片
			if ($_FILES['thumb']['tmp_name']) {
				$oldImg=db('article')->field('thumb')->find($data['id']);
				$imgPic=IMG_UPLOADS.$oldImg['thumb'];
				if (file_exists($imgPic)) {
					@unlink($imgPic);
				}
				$data['thumb']=$this->upload();
			}
			$data['updatetime']=date('Y-m-d H:i:s');
			//验证
			$validate =new ArticleValidate();
			if(!$validate->check($data)){
    			$this->error($validate->getError());
			}
			$save=db('article')->update($data);
			if($save!==false){
				$this->success('文章编辑成功','article/lists');
			}else{
				$this->error('文章编辑失败');
			}
			return;
		}
		$cateObj=db('cate');
		$cateRes=new Catetree();
		$cate=$cateObj->select();
		$cate=$cateRes->catetree($cate,$cateObj);
		$article=db('article')->where('id',input('id'))->find();
		$this->assign(array(
			'cate' =>$cate,
			'article'=>$article,
		));
		return view();
	}
	// 删除
	public function del($id){
		$oldImg=db('article')->field('thumb')->find($id);
				$imgPic=IMG_UPLOADS.$oldImg['thumb'];
				if (file_exists($imgPic)) {
					@unlink($imgPic);
				}
		$del=db('article')->delete($id);
		if($del){
				$this->success('文章删除成功','article/lists');
			}else{
				$this->error('文章删除失败');
		}
	}
	// 图片上传处理
	public  function  upload(){
	 // 获取表单上传文件 例如上传了001.jpg
    $file = request()->file('thumb');
    // 移动到框架应用根目录/public/uploads/ 目录下
	    if($file){
	        $info = $file->move(ROOT_PATH . 'public' . DS .'static'.DS. 'uploads');
	        if($info){
	            return $info->getSaveName();
	        }else{
	            // 上传失败获取错误信息
	            echo $file->getError();exit;
	        }
	    }
	}
	//ueditor图片管理
	public function imglist(){
		$_files=my_scandir();
		$files=array();
		foreach ($_files as $k => $v) {
			if (is_array($v)) {
				foreach ($v as $k1 => $v1) {
					$v1=str_replace(UEDITOR, HTTP_UEDITOR, $v1);
					$files[]=$v1;
				}
			}else{
				$v=str_replace(UEDITOR, HTTP_UEDITOR, $v);
				$files[]=$v;
			}
		}
		 $this->assign('files',$files);
		return view();
	}
	//资源图片删除
	public  function  delimg(){
		$img=input('imgsrc');
		$PicSrc=DEL_UEDITOR.$img;
		if (file_exists($PicSrc)) {
			if (@unlink($PicSrc)) {
				echo 1;exit;
			}else{
				echo 2;exit;
			}
		}else{
			echo 3;exit;
		}
	}
}
