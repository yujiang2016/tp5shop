<?php
namespace app\admin\validate;

use think\Validate;

class LinkValidate extends Validate
{
    protected $rule =   [
        'title'  => 'require|unique:link',
        'link_url'   => 'url',  
    ];
    
    protected $message  =   [
        'title.require' => '标题必须填写',
        'title.upique'     => '品标题不能重复',
        'link_url.url'   => '网址格式不正确',    
    ];
   
    
}