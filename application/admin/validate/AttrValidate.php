<?php
namespace app\admin\validate;

use think\Validate;

class AttrValidate extends Validate
{
    protected $rule =   [
        'attr_name'  => 'require|unipue:attr',
        'type_id' =>'require',  
        'attr_values' =>'require', 
    ];
    
    protected $message  =   [
        'attr_name.require' => '属性名称必须填写',
         'attr_name.unipue' => '属性名不得重复',
        'type_id.require' => '所属类型必须选择',  
        'attr_values.require' => '属性值不得为空',  
    ];
  
    
}