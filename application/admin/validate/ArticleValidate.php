<?php
namespace app\admin\validate;

use think\Validate;

class ArticleValidate extends Validate
{
    protected $rule =   [
        'title'  => 'require|unique:article', 
        'cate_id'  => 'require',   
        'emial'=>'emial',
        'link_url'=>'url',
    ];
    
    protected $message  =   [
        'title.require' => '文章标题必须填写',
        'title.upique'  => '文章标题不能重复',  
        'cate_id.require'  => '所属栏目不能为空',  
        'emial.emial'=>'邮箱格式不正确',
    ];
 
    
}