<?php
namespace app\admin\validate;

use think\Validate;

class ConfValidate extends Validate
{
    protected $rule =   [
        'cname'  => 'require|unique:conf', 
        'ename'  =>'require|unique:conf',
        '' 
    ];
    
    protected $message  =   [
        'cname.require' => '中文名称必须填写',
        'cname.upique'     => '中文名称不能重复', 
        'ename.require' => '英文名称必须填写',
        'ename.upique'     => '英文名称不能重复',    
    ];
   
    
}