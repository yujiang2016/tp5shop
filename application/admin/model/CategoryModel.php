<?php
namespace app\admin\model;
use think\Model;
/**
* 商品分类模型
*/
class CategoryModel extends Model
{
	
	protected $field=true;
	 protected $table = 'tp_category';
	protected static function init(){
		CategoryModel::beforeUpdate(function ($category) {
			// 商品id
			$categoryId=$category->id;
			$categoryDate=input('post.');
			// 处理商品推荐位
			db('item')->where(array('value_type'=>2,'value_id'=>$categoryId))->delete();
			if (isset($categoryDate['recpos'])) {
				foreach ($categoryDate['recpos'] as $k => $v) {
					db('item')->insert(['recpos_id'=>$v,'value_type'=>2,'value_id'=>$categoryId]);
				}
			}
		});
		CategoryModel::afterInsert(function ($category) {
			// 商品id
			$categoryId=$category->id;
			$categoryDate=input('post.');
			// 处理商品推荐位
			if (isset($categoryDate['recpos'])) {
				foreach ($categoryDate['recpos'] as $k => $v) {
					db('item')->insert(['recpos_id'=>$v,'value_type'=>2,'value_id'=>$categoryId]);
				}
			}
        });
	}
}