<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;
Route::rule('cate/:id','index/Cate/index','GET',['ext'=>'html|htm'],['id'=>'\d{1,3}']);
Route::rule('article/:id','index/Article/index','GET',['ext'=>'html|htm'],['id'=>'\d{1,3}']);
Route::rule('index','index/index/index');