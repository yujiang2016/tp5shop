<?php 
namespace app\index\model;
use think\Model;
/**
* 商品分类
*/
class CategoryModel extends Model
{	
	protected $table = 'tp_category';
	// 顶级和二级分类获取
	public function getCates(){
		$cateRes=$this->where('pid','=',0)->order('sort desc')->select();
		foreach ($cateRes as $k => $v) {
			$cateRes[$k]['children']=$this->where(array('pid'=>$v['id']))->select();
		}
		return $cateRes;
	}
}