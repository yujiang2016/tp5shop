<?php 
namespace app\index\model;
use think\Model;
/**
* 商品模型
*/
class GoodsModel extends Model
{
	public function getRecposGoods($recposId,$limit=''){
		// 热卖商品
		$recpos=db('item')->where(array('value_type'=>1,'recpos_id'=>$recposId))->select();
		static $hotId=array();
		foreach ($recpos as $k => $v) {
			$hotId[]=$v['value_id'];
		}
		$map['id']=array('IN',$hotId);
		$recRes=$this->field('id,mid_thumb,goods_name,shop_price,markte_price')->where($map)->limit($limit)->select();
		return $recRes;
	}
	
}
