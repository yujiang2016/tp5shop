<?php 
namespace app\index\model;
use think\Model;
/**
* 文章模块
*/
class Article extends Model
{
	//获取帮助分类
	public function getHelpArts(){
		$helpCateRes=model('Cate')->where(array('cate_type'=>3))->order('sort desc')->select();
		foreach ($helpCateRes as $k => $v) {
			$helpCateRes[$k]['arts']=model('Article')->where(array('cate_id'=>$v['id']))->select();
		}
		return $helpCateRes;
	}
	public function getArts($id,$limit){
		$arts=$this->where('cate_id','=',$id)->order('id DESC')->limit($limit)->select();
		return $arts;
	}
	 //获取网店信息
	 public function getShopInfo(){
	 	$shops=$this->where('cate_id','=',3)->select();
	 	return $shops;
	 }
}