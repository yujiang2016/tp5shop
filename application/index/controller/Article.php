<?php
namespace app\index\controller;
use think\Controller;
/**
* 文章
*/
class Article extends Base
{
	
	public function index($id){
		$art=db('article')->find($id);
		// 普通左侧栏目分类
		$comCates=model('cate')->getComCates();
		// 帮助左侧栏目分类
		$helpCates=model('cate')->ShopHelpCates();
		// 面包屑导航
		$postion=model('cate')->postion($art['cate_id']);
		$this->assign([
			'comCates'=>$comCates,
			'helpCates'=>$helpCates,
			'art'=>$art,
			'postion'=>$postion,
			]);
		return view('article');
	}
}