<?php
namespace app\index\controller;
use think\Controller;
/**
* 公共控制器
*/
class Base extends Controller
{
	public $confs;//配置项数组
	public function _initialize(){
		$this->_getHelpArts();//获取网站帮助信息并分配
		$this->getNav();//获取导航
		$this->_getConfs();//获取并分配配置项，为config赋值
		$this->_getShopInfo();//获取网店信息
		$this->_getCates();//获取顶级分类和二级分类
	}
	private  function _getCates(){
		$categoryRes=model('CategoryModel')->getCates();
		// dump($categoryRes);die;
		$this->assign([
			'categoryRes'=>$categoryRes,
			]);
	}
	private  function _getHelpArts(){
		$artModels=model('Article');
		$helpArts=$artModels->getHelpArts();
		$this->assign([
			'helpArts'=>$helpArts,
			]);
	}
	private function getNav(){
		$_navRes=db('nav')->order('sort DESC')->select();
		static $navRes=array();
		foreach ($_navRes as $k => $v) {
			$navRes[$v['pos']][]=$v;
		}
		$this->assign([
			'navRes'=>$navRes,
			]);
	}
	private function _getConfs(){
		$confRes=model('Conf')->getConfs();
		$this->confs=$confRes;
		$this->assign([
			'confRes'=>$confRes,
			]);
	}
	private function _getShopInfo(){
		$shopInfo=model('article')->getShopInfo();
		$this->assign([
			'shopInfo'=>$shopInfo,
			]);
	}
}