<?php
namespace app\index\controller;
use think\Controller;
use catetree\Catetree;
/**
* 分类
*/
class Cate extends Base
{
	
	public function index($id){
		// 获取当前栏目及子栏目
		 $cate=db('cate');
		$cateTree=new Catetree();
		$ids=$cateTree->childrenids($id,$cate);
		$ids[]=$id;
		$map['cate_id']=array('IN',$ids);
		// 当前栏目及其子栏目里的文章
		$artRes=db('article')->where($map)->select();
		// 当前栏目基本信息
		$cates=$cate->find($id);
		// 普通左侧栏目分类
		$comCates=model('cate')->getComCates();
		// 帮助左侧栏目分类
		$helpCates=model('cate')->ShopHelpCates();
		
		$this->assign([
			'comCates'=>$comCates,
			'helpCates'=>$helpCates,
			'cates'=>$cates,
			'artRes'=>$artRes,
			]);
		return view();
	}

	
}