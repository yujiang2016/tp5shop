/*
Navicat MySQL Data Transfer

Source Server         : www
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : tpshop

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-11-14 16:33:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tp_article
-- ----------------------------
DROP TABLE IF EXISTS `tp_article`;
CREATE TABLE `tp_article` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `title` varchar(30) NOT NULL COMMENT '标题',
  `keywords` varchar(60) NOT NULL COMMENT '关键词',
  `description` varchar(150) NOT NULL COMMENT '描述',
  `author` varchar(10) NOT NULL COMMENT '作者',
  `email` varchar(20) NOT NULL COMMENT '电子邮箱',
  `link_url` varchar(100) NOT NULL COMMENT '外链',
  `thumb` varchar(100) NOT NULL COMMENT '缩略图',
  `content` longtext NOT NULL COMMENT '内容',
  `show_top` tinyint(1) NOT NULL DEFAULT '0' COMMENT '置顶 1：是 0：否',
  `show_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '显示 1:是  0：否',
  `cate_id` smallint(6) NOT NULL COMMENT '所属栏目',
  `addtime` int(10) NOT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_article
-- ----------------------------
INSERT INTO `tp_article` VALUES ('13', '配送支付智能查询', '', '', '', '', '', '', '<p>配送支付智能查询</p>', '0', '1', '5', '1516087473');
INSERT INTO `tp_article` VALUES ('9', '售后流程', '', '', '', '', '', '', '<p>售后流程售后流程售后流程</p>', '0', '1', '4', '1516087423');
INSERT INTO `tp_article` VALUES ('10', '购物流程', '', '', '', '', '', '', '<p>购物流程</p>', '0', '1', '4', '1516087437');
INSERT INTO `tp_article` VALUES ('11', '订购方式', '', '', '', '', '', '', '<p>订购方式</p>', '0', '1', '4', '1516087448');
INSERT INTO `tp_article` VALUES ('12', '货到付款区域', '', '', '', '', '', '', '<p>货到付款区域11</p>', '0', '1', '5', '1516087461');
INSERT INTO `tp_article` VALUES ('14', '支付方式说明', '', '', '', '', '', '', '<p>支付方式说明</p>', '0', '1', '5', '1516087483');
INSERT INTO `tp_article` VALUES ('15', '资金管理', '', '', '', '', '', '', '<p>资金管理</p>', '0', '1', '7', '1516087493');
INSERT INTO `tp_article` VALUES ('16', '我的收藏', '', '', '', '', '', '', '<p>我的收藏</p>', '0', '1', '7', '1516087518');
INSERT INTO `tp_article` VALUES ('17', '我的订单', '', '', '', '', 'HTTP://tongpankt.com', '', '<p>我的订单</p>', '0', '1', '7', '1516087532');
INSERT INTO `tp_article` VALUES ('18', '退换货原则', '', '', '', '', '', '', '<p>退换货原则</p>', '0', '1', '16', '1516087542');
INSERT INTO `tp_article` VALUES ('19', '售后服务保证', '', '', '', '', '', '', '<p>售后服务保证</p>', '0', '1', '16', '1516087552');
INSERT INTO `tp_article` VALUES ('20', '产品质量保证', '', '', '', '', '', '', '<p>产品质量保证</p>', '0', '1', '16', '1516087562');
INSERT INTO `tp_article` VALUES ('21', '网站故障报告', '', '', '', '', '', '', '<p>网站故障报告</p>', '0', '1', '17', '1516087575');
INSERT INTO `tp_article` VALUES ('22', '选机咨询', '', '', '', '', '', '', '<p>选机咨询</p>', '0', '1', '17', '1516087591');
INSERT INTO `tp_article` VALUES ('23', '投诉与建议', '', '', '', '', '', '', '<p>投诉与建议</p>', '0', '1', '17', '1516087604');
INSERT INTO `tp_article` VALUES ('24', '隐私保护 ', '', '', '', '', '', '', '<p>隐私保护&nbsp;隐私保护&nbsp;隐私保护&nbsp;隐私保护&nbsp;</p>', '0', '1', '3', '1516245006');
INSERT INTO `tp_article` VALUES ('25', '联系我们 ', '', '', '', '', '', '', '<p>联系我们&nbsp;联系我们&nbsp;联系我们&nbsp;联系我们&nbsp;联系我们&nbsp;联系我们&nbsp;</p>', '0', '1', '3', '1516245025');
INSERT INTO `tp_article` VALUES ('26', '免责条款 ', '', '', '', '', '', '', '<p>免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;</p>', '0', '1', '3', '1516245037');
INSERT INTO `tp_article` VALUES ('27', '公司简介 ', '', '', '', '', '', '', '<p>公司简介&nbsp;公司简介&nbsp;公司简介&nbsp;公司简介&nbsp;公司简介&nbsp;公司简介&nbsp;公司简介&nbsp;公司简介&nbsp;</p>', '0', '1', '3', '1516245050');
INSERT INTO `tp_article` VALUES ('28', '意见反馈', '', '', '', '', '', '', '<p>意见反馈意见反馈意见反馈意见反馈意见反馈意见反馈意见反馈意见反馈意见反馈</p>', '0', '1', '3', '1516245066');
INSERT INTO `tp_article` VALUES ('29', '服务店突破2000多家', '', '', '', '', '', '', '<p>服务店突破2000多家</p>', '0', '1', '20', '1516869845');
INSERT INTO `tp_article` VALUES ('30', '我们成为中国最大家电零售B2B2C开源电商系统', '', '', '', '', '', '', '<p>我们成为中国最大家电零售B2B2C开源电商系统</p>', '0', '1', '20', '1516869890');
INSERT INTO `tp_article` VALUES ('31', '三大国际腕表品牌签约', '', '', '', '', '', '', '<p>三大国际腕表品牌签约三大国际腕表品牌签约三大国际腕表品牌签约</p>', '0', '1', '20', '1516869915');
INSERT INTO `tp_article` VALUES ('32', '春季家装季，家电买一送一', '', '', '', '', '', '', '<p>春季家装季，家电买一送一春季家装季，家电买一送一春季家装季，家电买一送一</p>', '0', '1', '26', '1516869930');
INSERT INTO `tp_article` VALUES ('33', '抢百元优惠券，享4.22%活期', '', '', '', '', '', '', '<p>抢百元优惠券，享4.22%活期</p>', '0', '1', '26', '1516869942');
INSERT INTO `tp_article` VALUES ('34', 'Macbook最高返50000消费豆', '', '', '', '', '', '', '<p>Macbook最高返50000消费豆Macbook最高返50000消费豆Macbook最高返50000消费豆</p>', '0', '1', '26', '1516869963');

-- ----------------------------
-- Table structure for tp_attr
-- ----------------------------
DROP TABLE IF EXISTS `tp_attr`;
CREATE TABLE `tp_attr` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `type_id` smallint(6) NOT NULL COMMENT '商品类型id',
  `attr_name` varchar(30) NOT NULL COMMENT '属性名称',
  `attr_type` varchar(60) NOT NULL DEFAULT '1' COMMENT '属性类型，1单选，2唯一',
  `attr_values` varchar(100) NOT NULL COMMENT '属性值',
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_attr
-- ----------------------------
INSERT INTO `tp_attr` VALUES ('5', '2', '内存', '1', '128G,256G');
INSERT INTO `tp_attr` VALUES ('4', '2', '颜色', '1', '红色,黑色');
INSERT INTO `tp_attr` VALUES ('7', '2', '厂家', '2', '');
INSERT INTO `tp_attr` VALUES ('8', '3', '尺码', '1', '商务,时尚');
INSERT INTO `tp_attr` VALUES ('9', '3', '厂家', '2', '');

-- ----------------------------
-- Table structure for tp_brand
-- ----------------------------
DROP TABLE IF EXISTS `tp_brand`;
CREATE TABLE `tp_brand` (
  `id` smallint(11) unsigned NOT NULL AUTO_INCREMENT,
  `brand_logo` varchar(255) NOT NULL COMMENT '品牌logo',
  `brand_name` varchar(60) NOT NULL COMMENT '品牌名称',
  `brand_url` varchar(60) NOT NULL COMMENT '品牌地址',
  `brand_description` varchar(100) NOT NULL COMMENT '品牌描述',
  `sort` int(10) NOT NULL DEFAULT '50' COMMENT '排序',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1：显示，0隐藏',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_brand
-- ----------------------------
INSERT INTO `tp_brand` VALUES ('1', '20180830\\373843e6686698eb6a8006dde071f63f.jpg', 'vivo', 'http://www.vivo.com', 'vivo最新', '50', '1');
INSERT INTO `tp_brand` VALUES ('3', '20180830\\b0191496c24a2bc32932076119a9ca1e.jpg', 'oppo', 'www.oppo.com', 'dc', '50', '1');
INSERT INTO `tp_brand` VALUES ('4', '20180830\\8e84ae42779596a969667c3dccd49a50.jpg', 'baidu', 'http://www.vaidu.com', '显示', '50', '0');
INSERT INTO `tp_brand` VALUES ('6', '', '波司登', 'http://www.baidu.com', '', '50', '1');

-- ----------------------------
-- Table structure for tp_cate
-- ----------------------------
DROP TABLE IF EXISTS `tp_cate`;
CREATE TABLE `tp_cate` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT '栏目id',
  `cate_name` varchar(20) NOT NULL COMMENT '栏目名称',
  `cate_type` tinyint(1) NOT NULL DEFAULT '5' COMMENT '栏目类型 cate_type 1:系统分类    2：帮助分类    3：网店帮助    4：网店信息    5：普通分类',
  `keywords` varchar(100) NOT NULL COMMENT '关键词',
  `description` varchar(150) NOT NULL COMMENT '描述',
  `show_nav` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示到导航栏 1:显示到导航栏 0：不显示到导航栏',
  `allow_son` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1：是 0：否 是否允许添加子分类',
  `sort` smallint(6) NOT NULL DEFAULT '50' COMMENT '排序',
  `pid` smallint(6) NOT NULL DEFAULT '0' COMMENT '上级栏目id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_cate
-- ----------------------------
INSERT INTO `tp_cate` VALUES ('1', '系统', '1', '系统', '系统', '0', '0', '7', '0');
INSERT INTO `tp_cate` VALUES ('2', '网店帮助分类', '2', '网店帮助分类1', '网店帮助分类1', '0', '1', '11', '1');
INSERT INTO `tp_cate` VALUES ('3', '网店信息', '4', '网店信息', '网店信息', '0', '0', '12', '1');
INSERT INTO `tp_cate` VALUES ('4', '新手上路 ', '3', '新手上路 ', '新手上路 ', '0', '0', '55', '2');
INSERT INTO `tp_cate` VALUES ('5', '配送与支付 ', '3', '配送与支付 ', '配送与支付 ', '0', '0', '54', '2');
INSERT INTO `tp_cate` VALUES ('7', '会员中心', '3', '会员中心', '会员中心', '0', '0', '53', '2');
INSERT INTO `tp_cate` VALUES ('16', '服务保证', '3', '', '', '0', '1', '52', '2');
INSERT INTO `tp_cate` VALUES ('17', '联系我们', '3', '', '', '0', '1', '51', '2');
INSERT INTO `tp_cate` VALUES ('18', '3G资讯', '5', '', '', '0', '1', '6', '0');
INSERT INTO `tp_cate` VALUES ('19', '站内快讯', '5', '', '', '0', '1', '5', '0');
INSERT INTO `tp_cate` VALUES ('20', '商城公告', '5', '', '', '0', '1', '4', '0');
INSERT INTO `tp_cate` VALUES ('21', '发票问题', '5', '', '', '0', '1', '3', '0');
INSERT INTO `tp_cate` VALUES ('22', '微分销', '5', '', '', '0', '1', '1', '0');
INSERT INTO `tp_cate` VALUES ('23', 'App下载', '5', '', '', '0', '1', '2', '0');
INSERT INTO `tp_cate` VALUES ('24', 'IOS版', '5', '', '', '0', '1', '50', '23');
INSERT INTO `tp_cate` VALUES ('25', '安卓版', '5', '', '', '0', '1', '50', '23');
INSERT INTO `tp_cate` VALUES ('26', '促销信息', '5', '', '', '0', '1', '50', '0');

-- ----------------------------
-- Table structure for tp_category
-- ----------------------------
DROP TABLE IF EXISTS `tp_category`;
CREATE TABLE `tp_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(30) NOT NULL COMMENT '商品分类名称',
  `cate_img` varchar(100) NOT NULL COMMENT '缩略图',
  `pid` smallint(6) NOT NULL DEFAULT '0' COMMENT '上级id',
  `sort` smallint(6) NOT NULL DEFAULT '50' COMMENT '排序',
  `keywords` varchar(100) NOT NULL COMMENT '关键字',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `show_cate` int(1) NOT NULL DEFAULT '0' COMMENT '1显示，0隐藏',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_category
-- ----------------------------
INSERT INTO `tp_category` VALUES ('11', '童装', '20180903\\4ac23b3ddad86932bf7a7fa4b1425500.jpg', '11', '50', '', '', '1');
INSERT INTO `tp_category` VALUES ('2', '女装', '20180903\\5732ec0e0111070c3670c0b598e22eae.jpg', '2', '1', '', '', '0');
INSERT INTO `tp_category` VALUES ('12', '男装', '20180903\\1c4a08528f789cdacf3b8309ba56ae32.jpg', '12', '50', '', '', '0');
INSERT INTO `tp_category` VALUES ('10', '男装', '20180903\\1916637f2f75bf2d57ffc25f3726508d.jpg', '10', '50', '', '', '0');
INSERT INTO `tp_category` VALUES ('13', '男装', '20180903\\5b75e8f1dc5368bf101774e4fec25333.jpg', '13', '50', '', '', '1');
INSERT INTO `tp_category` VALUES ('14', '童装', '20180903\\59e9219f6be2e42322aa0495703d6e42.jpg', '14', '50', '嗯嗯 ', '嗯嗯', '0');
INSERT INTO `tp_category` VALUES ('15', '男装', '20180903\\b51224fb4280540d20ee1cb9255a8719.jpg', '0', '50', '', '', '1');
INSERT INTO `tp_category` VALUES ('16', '女装', '20180903\\3dfe4f74366fdc20e4e6658fb6003efc.jpg', '0', '50', '', '', '1');
INSERT INTO `tp_category` VALUES ('17', '电脑', '', '0', '50', '', '', '0');

-- ----------------------------
-- Table structure for tp_conf
-- ----------------------------
DROP TABLE IF EXISTS `tp_conf`;
CREATE TABLE `tp_conf` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `ename` varchar(30) NOT NULL COMMENT '英文名称',
  `cname` varchar(30) NOT NULL COMMENT '中文名称',
  `form_type` varchar(10) NOT NULL DEFAULT 'input' COMMENT '表单类型',
  `conf_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '配置类型 1：网店配置 2：商品配置',
  `values` varchar(60) NOT NULL COMMENT '可选值',
  `value` varchar(255) NOT NULL COMMENT '默认值',
  `sort` smallint(6) NOT NULL DEFAULT '50',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_conf
-- ----------------------------
INSERT INTO `tp_conf` VALUES ('2', 'site_name', '站点名称', 'input', '1', '', '我的网站', '7');
INSERT INTO `tp_conf` VALUES ('3', 'site_keywords', '站点关键词', 'input', '1', '', '网站', '6');
INSERT INTO `tp_conf` VALUES ('4', 'site_description', '站点描述', 'textarea', '1', '', '网站网站网站网站网站网站网站网站网站网站网站', '5');
INSERT INTO `tp_conf` VALUES ('5', 'close_site', '关闭站点', 'radio', '1', '是,否', '否', '4');
INSERT INTO `tp_conf` VALUES ('6', 'logo', '网站logo', 'file', '1', '', '20171122\\2abdd4ed4c290faeb73437387ea9593c.jpg', '3');
INSERT INTO `tp_conf` VALUES ('7', 'reg', '会员注册', 'select', '1', '开启,关闭', '开启', '2');
INSERT INTO `tp_conf` VALUES ('8', 'checks', '多选测试', 'checkbox', '1', '选项1,选项2,选项3,选项4', '选项1,选项3', '1');
INSERT INTO `tp_conf` VALUES ('10', 'ewm', '二维码', 'file', '1', '', '20171122\\556f518e5123d50477e21030771926ac.jpg', '4');
INSERT INTO `tp_conf` VALUES ('11', 'ces', '测试复选', 'checkbox', '1', '测试1,测试2,测试3,测试4', '测试2,测试4', '0');
INSERT INTO `tp_conf` VALUES ('12', 'cs', '测试', 'input', '2', '', '测试1111', '50');
INSERT INTO `tp_conf` VALUES ('13', 'ts', '每页条数', 'checkbox', '2', '5,10,15', '10', '50');
INSERT INTO `tp_conf` VALUES ('14', 'search_keywords', '搜索关键词', 'textarea', '1', '', '周大福,内衣,Five,Plus,手机', '50');
INSERT INTO `tp_conf` VALUES ('15', 'search_value', '搜索框默认值', 'input', '1', '', 'mackpro', '50');
INSERT INTO `tp_conf` VALUES ('16', 'cache', '开启缓存', 'radio', '1', '是,否', '否', '50');
INSERT INTO `tp_conf` VALUES ('17', 'cache_time', '缓存有效期', 'input', '1', '', '20', '50');

-- ----------------------------
-- Table structure for tp_goods
-- ----------------------------
DROP TABLE IF EXISTS `tp_goods`;
CREATE TABLE `tp_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(50) NOT NULL COMMENT '商品名称',
  `goods_code` varchar(16) NOT NULL COMMENT '商品编码',
  `og_thumb` varchar(100) NOT NULL COMMENT '商品原图',
  `sm_thumb` varchar(100) NOT NULL COMMENT '商品小图',
  `mid_thumb` varchar(100) NOT NULL COMMENT '商品中图',
  `big_thumb` varchar(100) NOT NULL COMMENT '商品大图',
  `markte_price` decimal(10,2) NOT NULL COMMENT '市场价',
  `shop_price` decimal(10,2) NOT NULL COMMENT '商品价格',
  `on_sale` tinyint(1) NOT NULL COMMENT '是否上架',
  `category_id` mediumint(9) NOT NULL COMMENT '所属类型id',
  `brand_id` mediumint(9) NOT NULL DEFAULT '0' COMMENT '商品品牌id',
  `type_id` mediumint(9) NOT NULL DEFAULT '0' COMMENT '商品类型id',
  `goods_des` longtext NOT NULL COMMENT '商品描述',
  `goods_weight` decimal(10,4) NOT NULL COMMENT '商品重量',
  `weight_unit` varchar(10) NOT NULL DEFAULT 'kg' COMMENT '单位',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`,`brand_id`,`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_goods
-- ----------------------------
INSERT INTO `tp_goods` VALUES ('39', 'T恤', '1541472203442572', '20181106\\6666053ab83e0acdb4e8cd0f1e41201a.jpg', '20181106\\sm_6666053ab83e0acdb4e8cd0f1e41201a.jpg', '20181106\\mid_6666053ab83e0acdb4e8cd0f1e41201a.jpg', '20181106\\big_6666053ab83e0acdb4e8cd0f1e41201a.jpg', '99.00', '89.00', '1', '15', '6', '3', '<p>地方的</p>', '10.0000', 'kg');
INSERT INTO `tp_goods` VALUES ('40', '鞋', '1541472272978081', '20181106\\27f0aa6acfb25f81d5f14d26cb90918c.jpg', '20181106\\sm_27f0aa6acfb25f81d5f14d26cb90918c.jpg', '20181106\\mid_27f0aa6acfb25f81d5f14d26cb90918c.jpg', '20181106\\big_27f0aa6acfb25f81d5f14d26cb90918c.jpg', '128.00', '118.00', '1', '17', '1', '0', '<p>大富大贵发的发的大幅度发</p>', '20.0000', 'kg');
INSERT INTO `tp_goods` VALUES ('41', '测试12', '1541492014332573', '20181106\\0f7531b7e81a3bfaa632235ff1136e9b.jpg', '20181106\\sm_0f7531b7e81a3bfaa632235ff1136e9b.jpg', '20181106\\mid_0f7531b7e81a3bfaa632235ff1136e9b.jpg', '20181106\\big_0f7531b7e81a3bfaa632235ff1136e9b.jpg', '688.00', '678.00', '1', '15', '6', '3', '<p>写错不是的地方第三方</p>', '20.0000', 'kg');

-- ----------------------------
-- Table structure for tp_goods_attr
-- ----------------------------
DROP TABLE IF EXISTS `tp_goods_attr`;
CREATE TABLE `tp_goods_attr` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `attr_id` mediumint(9) NOT NULL COMMENT '属性id',
  `attr_value` varchar(150) NOT NULL COMMENT '属性值',
  `attr_price` decimal(20,0) NOT NULL DEFAULT '0' COMMENT '属性价格',
  `goods_id` int(10) NOT NULL COMMENT '所属商品',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COMMENT='商品属性表';

-- ----------------------------
-- Records of tp_goods_attr
-- ----------------------------
INSERT INTO `tp_goods_attr` VALUES ('63', '8', '商务', '100', '39');
INSERT INTO `tp_goods_attr` VALUES ('64', '8', '时尚', '80', '39');
INSERT INTO `tp_goods_attr` VALUES ('65', '5', '128G', '128', '40');
INSERT INTO `tp_goods_attr` VALUES ('66', '4', '红色', '118', '40');
INSERT INTO `tp_goods_attr` VALUES ('67', '7', '地方', '0', '40');
INSERT INTO `tp_goods_attr` VALUES ('74', '8', '商务', '10', '41');
INSERT INTO `tp_goods_attr` VALUES ('75', '8', '时尚', '20', '41');
INSERT INTO `tp_goods_attr` VALUES ('76', '9', '包头', '0', '41');

-- ----------------------------
-- Table structure for tp_goods_photo
-- ----------------------------
DROP TABLE IF EXISTS `tp_goods_photo`;
CREATE TABLE `tp_goods_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(5) NOT NULL DEFAULT '0' COMMENT '商品id',
  `sm_photo` varchar(255) NOT NULL DEFAULT '' COMMENT '小图',
  `mid_photo` varchar(255) NOT NULL DEFAULT '' COMMENT '中图',
  `big_photo` varchar(255) NOT NULL DEFAULT '' COMMENT '大图',
  `og_photo` varchar(255) DEFAULT NULL COMMENT '原图',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_goods_photo
-- ----------------------------
INSERT INTO `tp_goods_photo` VALUES ('34', '39', '20181106\\sm_a54090c1b8efcef8dd8f660495a07ec1.jpg', '20181106\\mid_a54090c1b8efcef8dd8f660495a07ec1.jpg', '20181106\\big_a54090c1b8efcef8dd8f660495a07ec1.jpg', '20181106\\a54090c1b8efcef8dd8f660495a07ec1.jpg');
INSERT INTO `tp_goods_photo` VALUES ('39', '41', '20181106\\sm_0c191fc13cc18fd69b37137ba7db4f24.jpg', '20181106\\mid_0c191fc13cc18fd69b37137ba7db4f24.jpg', '20181106\\big_0c191fc13cc18fd69b37137ba7db4f24.jpg', '20181106\\0c191fc13cc18fd69b37137ba7db4f24.jpg');
INSERT INTO `tp_goods_photo` VALUES ('38', '40', '20181106\\sm_75a6e1ba4a26fad4e062c0f940f42ff7.jpg', '20181106\\mid_75a6e1ba4a26fad4e062c0f940f42ff7.jpg', '20181106\\big_75a6e1ba4a26fad4e062c0f940f42ff7.jpg', '20181106\\75a6e1ba4a26fad4e062c0f940f42ff7.jpg');

-- ----------------------------
-- Table structure for tp_link
-- ----------------------------
DROP TABLE IF EXISTS `tp_link`;
CREATE TABLE `tp_link` (
  `id` smallint(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL COMMENT '标题',
  `link_url` varchar(60) NOT NULL COMMENT '链接地址',
  `logo` varchar(100) NOT NULL COMMENT 'logo图片',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态，1：启用，0：禁用',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '类型，1：文字，2：图片类型',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_link
-- ----------------------------

-- ----------------------------
-- Table structure for tp_member_level
-- ----------------------------
DROP TABLE IF EXISTS `tp_member_level`;
CREATE TABLE `tp_member_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level_name` varchar(30) DEFAULT NULL COMMENT '会员级别',
  `bom_point` int(11) DEFAULT NULL COMMENT '积分下线',
  `top_point` int(11) DEFAULT NULL COMMENT '积分上线',
  `rate` varchar(255) DEFAULT NULL COMMENT '折扣率',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_member_level
-- ----------------------------
INSERT INTO `tp_member_level` VALUES ('1', '注册会员', '0', '2000', '100');
INSERT INTO `tp_member_level` VALUES ('2', '高级会员', '0', '15000', '95');
INSERT INTO `tp_member_level` VALUES ('3', 'vip会员', '0', '300000', '70');

-- ----------------------------
-- Table structure for tp_member_price
-- ----------------------------
DROP TABLE IF EXISTS `tp_member_price`;
CREATE TABLE `tp_member_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mprice` decimal(10,2) DEFAULT '0.00' COMMENT '会员价格',
  `mlevel_id` int(11) DEFAULT NULL COMMENT '会员级别id',
  `goods_id` int(11) DEFAULT NULL COMMENT '商品id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_member_price
-- ----------------------------
INSERT INTO `tp_member_price` VALUES ('64', '99.00', '1', '39');
INSERT INTO `tp_member_price` VALUES ('65', '88.00', '2', '39');
INSERT INTO `tp_member_price` VALUES ('66', '78.00', '3', '39');
INSERT INTO `tp_member_price` VALUES ('74', '168.00', '3', '40');
INSERT INTO `tp_member_price` VALUES ('73', '178.00', '2', '40');
INSERT INTO `tp_member_price` VALUES ('72', '188.00', '1', '40');
INSERT INTO `tp_member_price` VALUES ('86', '666.00', '3', '41');
INSERT INTO `tp_member_price` VALUES ('85', '678.00', '2', '41');
INSERT INTO `tp_member_price` VALUES ('84', '688.00', '1', '41');

-- ----------------------------
-- Table structure for tp_nav
-- ----------------------------
DROP TABLE IF EXISTS `tp_nav`;
CREATE TABLE `tp_nav` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `nav_name` varchar(45) NOT NULL DEFAULT '' COMMENT '导航名称',
  `nav_url` varchar(50) NOT NULL COMMENT '导航地址',
  `open` tinyint(1) NOT NULL DEFAULT '1' COMMENT '打开方式：1新窗口，2当前页',
  `pos` varchar(30) NOT NULL DEFAULT '' COMMENT '导航位置 顶部：top,中间：mid,底部：bottom',
  `sort` smallint(3) NOT NULL DEFAULT '50' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_nav
-- ----------------------------
INSERT INTO `tp_nav` VALUES ('1', '我的订单', 'http://www.baidu.com', '2', 'top', '14');
INSERT INTO `tp_nav` VALUES ('2', '我的浏览', 'http://www.baidu.com', '1', 'top', '13');
INSERT INTO `tp_nav` VALUES ('3', '我的收藏', 'http://www.baidu.com', '1', 'top', '12');
INSERT INTO `tp_nav` VALUES ('4', '客户服务', 'http://www.baidu.com', '1', 'top', '11');
INSERT INTO `tp_nav` VALUES ('5', '女人街', 'http://www.baidu.com', '1', 'mid', '50');
INSERT INTO `tp_nav` VALUES ('6', '男人柜', 'http://www.baidu.com', '1', 'mid', '50');
INSERT INTO `tp_nav` VALUES ('7', '品牌专区', 'http://www.baidu.com', '1', 'mid', '50');
INSERT INTO `tp_nav` VALUES ('8', '聚划算', 'http://www.baidu.com', '1', 'mid', '50');
INSERT INTO `tp_nav` VALUES ('9', '积分商城', 'http://www.baidu.com', '1', 'mid', '50');

-- ----------------------------
-- Table structure for tp_product
-- ----------------------------
DROP TABLE IF EXISTS `tp_product`;
CREATE TABLE `tp_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_attr` varchar(255) NOT NULL DEFAULT '' COMMENT '商品属性',
  `goods_id` int(11) NOT NULL COMMENT '商品id',
  `goods_number` int(11) NOT NULL DEFAULT '0' COMMENT '商品库存量',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_product
-- ----------------------------

-- ----------------------------
-- Table structure for tp_recpos
-- ----------------------------
DROP TABLE IF EXISTS `tp_recpos`;
CREATE TABLE `tp_recpos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rec_type` int(11) NOT NULL DEFAULT '1' COMMENT '推荐位类型 1：商品 2：分类',
  `rec_name` varchar(60) NOT NULL DEFAULT '' COMMENT '推荐位名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_recpos
-- ----------------------------
INSERT INTO `tp_recpos` VALUES ('1', '1', '热卖商品');
INSERT INTO `tp_recpos` VALUES ('2', '1', '限时抢购');
INSERT INTO `tp_recpos` VALUES ('3', '1', '新品推荐');
INSERT INTO `tp_recpos` VALUES ('4', '2', '首页推荐');
INSERT INTO `tp_recpos` VALUES ('5', '2', '推荐分类');
INSERT INTO `tp_recpos` VALUES ('6', '1', '精品推荐');
INSERT INTO `tp_recpos` VALUES ('7', '1', '首页商品');

-- ----------------------------
-- Table structure for tp_type
-- ----------------------------
DROP TABLE IF EXISTS `tp_type`;
CREATE TABLE `tp_type` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(60) NOT NULL COMMENT '商品类型名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_type
-- ----------------------------
INSERT INTO `tp_type` VALUES ('2', '笔记本');
INSERT INTO `tp_type` VALUES ('3', '男装');
