/*
Navicat MySQL Data Transfer

Source Server         : www
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : tp5

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-11-14 15:59:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tp_conf
-- ----------------------------
DROP TABLE IF EXISTS `tp_conf`;
CREATE TABLE `tp_conf` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `ename` varchar(30) NOT NULL COMMENT '英文名称',
  `cname` varchar(30) NOT NULL COMMENT '中文名称',
  `form_type` varchar(10) NOT NULL DEFAULT 'input' COMMENT '表单类型',
  `conf_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '配置类型 1：网店配置 2：商品配置',
  `values` varchar(60) NOT NULL COMMENT '可选值',
  `value` varchar(255) NOT NULL COMMENT '默认值',
  `sort` smallint(6) NOT NULL DEFAULT '50',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_conf
-- ----------------------------
INSERT INTO `tp_conf` VALUES ('2', 'site_name', '站点名称', 'input', '1', '', '我的网站', '7');
INSERT INTO `tp_conf` VALUES ('3', 'site_keywords', '站点关键词', 'input', '1', '', '网站', '6');
INSERT INTO `tp_conf` VALUES ('4', 'site_description', '站点描述', 'textarea', '1', '', '网站网站网站网站网站网站网站网站网站网站网站', '5');
INSERT INTO `tp_conf` VALUES ('5', 'close_site', '关闭站点', 'radio', '1', '是,否', '否', '4');
INSERT INTO `tp_conf` VALUES ('6', 'logo', '网站logo', 'file', '1', '', '20171122\\2abdd4ed4c290faeb73437387ea9593c.jpg', '3');
INSERT INTO `tp_conf` VALUES ('7', 'reg', '会员注册', 'select', '1', '开启,关闭', '开启', '2');
INSERT INTO `tp_conf` VALUES ('8', 'checks', '多选测试', 'checkbox', '1', '选项1,选项2,选项3,选项4', '选项1,选项3', '1');
INSERT INTO `tp_conf` VALUES ('10', 'ewm', '二维码', 'file', '1', '', '20171122\\556f518e5123d50477e21030771926ac.jpg', '4');
INSERT INTO `tp_conf` VALUES ('11', 'ces', '测试复选', 'checkbox', '1', '测试1,测试2,测试3,测试4', '测试2,测试4', '0');
INSERT INTO `tp_conf` VALUES ('12', 'cs', '测试', 'input', '2', '', '测试1111', '50');
INSERT INTO `tp_conf` VALUES ('13', 'ts', '每页条数', 'checkbox', '2', '5,10,15', '10', '50');
INSERT INTO `tp_conf` VALUES ('14', 'search_keywords', '搜索关键词', 'textarea', '1', '', '周大福,内衣,Five,Plus,手机', '50');
INSERT INTO `tp_conf` VALUES ('15', 'search_value', '搜索框默认值', 'input', '1', '', 'mackpro', '50');
INSERT INTO `tp_conf` VALUES ('16', 'cache', '开启缓存', 'radio', '1', '是,否', '否', '50');
INSERT INTO `tp_conf` VALUES ('17', 'cache_time', '缓存有效期', 'input', '1', '', '20', '50');
