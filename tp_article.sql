/*
Navicat MySQL Data Transfer

Source Server         : www
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : tp5

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-11-12 16:43:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tp_article
-- ----------------------------
DROP TABLE IF EXISTS `tp_article`;
CREATE TABLE `tp_article` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `title` varchar(30) NOT NULL COMMENT '标题',
  `keywords` varchar(60) NOT NULL COMMENT '关键词',
  `description` varchar(150) NOT NULL COMMENT '描述',
  `author` varchar(10) NOT NULL COMMENT '作者',
  `email` varchar(20) NOT NULL COMMENT '电子邮箱',
  `link_url` varchar(100) NOT NULL COMMENT '外链',
  `thumb` varchar(100) NOT NULL COMMENT '缩略图',
  `content` longtext NOT NULL COMMENT '内容',
  `show_top` tinyint(1) NOT NULL DEFAULT '0' COMMENT '置顶 1：是 0：否',
  `show_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '显示 1:是  0：否',
  `cate_id` smallint(6) NOT NULL COMMENT '所属栏目',
  `addtime` int(10) NOT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_article
-- ----------------------------
INSERT INTO `tp_article` VALUES ('13', '配送支付智能查询', '', '', '', '', '', '', '<p>配送支付智能查询</p>', '0', '1', '5', '1516087473');
INSERT INTO `tp_article` VALUES ('9', '售后流程', '', '', '', '', '', '', '<p>售后流程售后流程售后流程</p>', '0', '1', '4', '1516087423');
INSERT INTO `tp_article` VALUES ('10', '购物流程', '', '', '', '', '', '', '<p>购物流程</p>', '0', '1', '4', '1516087437');
INSERT INTO `tp_article` VALUES ('11', '订购方式', '', '', '', '', '', '', '<p>订购方式</p>', '0', '1', '4', '1516087448');
INSERT INTO `tp_article` VALUES ('12', '货到付款区域', '', '', '', '', '', '', '<p>货到付款区域11</p>', '0', '1', '5', '1516087461');
INSERT INTO `tp_article` VALUES ('14', '支付方式说明', '', '', '', '', '', '', '<p>支付方式说明</p>', '0', '1', '5', '1516087483');
INSERT INTO `tp_article` VALUES ('15', '资金管理', '', '', '', '', '', '', '<p>资金管理</p>', '0', '1', '7', '1516087493');
INSERT INTO `tp_article` VALUES ('16', '我的收藏', '', '', '', '', '', '', '<p>我的收藏</p>', '0', '1', '7', '1516087518');
INSERT INTO `tp_article` VALUES ('17', '我的订单', '', '', '', '', 'HTTP://tongpankt.com', '', '<p>我的订单</p>', '0', '1', '7', '1516087532');
INSERT INTO `tp_article` VALUES ('18', '退换货原则', '', '', '', '', '', '', '<p>退换货原则</p>', '0', '1', '16', '1516087542');
INSERT INTO `tp_article` VALUES ('19', '售后服务保证', '', '', '', '', '', '', '<p>售后服务保证</p>', '0', '1', '16', '1516087552');
INSERT INTO `tp_article` VALUES ('20', '产品质量保证', '', '', '', '', '', '', '<p>产品质量保证</p>', '0', '1', '16', '1516087562');
INSERT INTO `tp_article` VALUES ('21', '网站故障报告', '', '', '', '', '', '', '<p>网站故障报告</p>', '0', '1', '17', '1516087575');
INSERT INTO `tp_article` VALUES ('22', '选机咨询', '', '', '', '', '', '', '<p>选机咨询</p>', '0', '1', '17', '1516087591');
INSERT INTO `tp_article` VALUES ('23', '投诉与建议', '', '', '', '', '', '', '<p>投诉与建议</p>', '0', '1', '17', '1516087604');
INSERT INTO `tp_article` VALUES ('24', '隐私保护 ', '', '', '', '', '', '', '<p>隐私保护&nbsp;隐私保护&nbsp;隐私保护&nbsp;隐私保护&nbsp;</p>', '0', '1', '3', '1516245006');
INSERT INTO `tp_article` VALUES ('25', '联系我们 ', '', '', '', '', '', '', '<p>联系我们&nbsp;联系我们&nbsp;联系我们&nbsp;联系我们&nbsp;联系我们&nbsp;联系我们&nbsp;</p>', '0', '1', '3', '1516245025');
INSERT INTO `tp_article` VALUES ('26', '免责条款 ', '', '', '', '', '', '', '<p>免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;免责条款&nbsp;</p>', '0', '1', '3', '1516245037');
INSERT INTO `tp_article` VALUES ('27', '公司简介 ', '', '', '', '', '', '', '<p>公司简介&nbsp;公司简介&nbsp;公司简介&nbsp;公司简介&nbsp;公司简介&nbsp;公司简介&nbsp;公司简介&nbsp;公司简介&nbsp;</p>', '0', '1', '3', '1516245050');
INSERT INTO `tp_article` VALUES ('28', '意见反馈', '', '', '', '', '', '', '<p>意见反馈意见反馈意见反馈意见反馈意见反馈意见反馈意见反馈意见反馈意见反馈</p>', '0', '1', '3', '1516245066');
INSERT INTO `tp_article` VALUES ('29', '服务店突破2000多家', '', '', '', '', '', '', '<p>服务店突破2000多家</p>', '0', '1', '20', '1516869845');
INSERT INTO `tp_article` VALUES ('30', '我们成为中国最大家电零售B2B2C开源电商系统', '', '', '', '', '', '', '<p>我们成为中国最大家电零售B2B2C开源电商系统</p>', '0', '1', '20', '1516869890');
INSERT INTO `tp_article` VALUES ('31', '三大国际腕表品牌签约', '', '', '', '', '', '', '<p>三大国际腕表品牌签约三大国际腕表品牌签约三大国际腕表品牌签约</p>', '0', '1', '20', '1516869915');
INSERT INTO `tp_article` VALUES ('32', '春季家装季，家电买一送一', '', '', '', '', '', '', '<p>春季家装季，家电买一送一春季家装季，家电买一送一春季家装季，家电买一送一</p>', '0', '1', '26', '1516869930');
INSERT INTO `tp_article` VALUES ('33', '抢百元优惠券，享4.22%活期', '', '', '', '', '', '', '<p>抢百元优惠券，享4.22%活期</p>', '0', '1', '26', '1516869942');
INSERT INTO `tp_article` VALUES ('34', 'Macbook最高返50000消费豆', '', '', '', '', '', '', '<p>Macbook最高返50000消费豆Macbook最高返50000消费豆Macbook最高返50000消费豆</p>', '0', '1', '26', '1516869963');
