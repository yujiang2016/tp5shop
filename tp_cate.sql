/*
Navicat MySQL Data Transfer

Source Server         : www
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : tp5

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-11-12 16:45:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tp_cate
-- ----------------------------
DROP TABLE IF EXISTS `tp_cate`;
CREATE TABLE `tp_cate` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT '栏目id',
  `cate_name` varchar(20) NOT NULL COMMENT '栏目名称',
  `cate_type` tinyint(1) NOT NULL DEFAULT '5' COMMENT '栏目类型 cate_type 1:系统分类    2：帮助分类    3：网店帮助    4：网店信息    5：普通分类',
  `keywords` varchar(100) NOT NULL COMMENT '关键词',
  `description` varchar(150) NOT NULL COMMENT '描述',
  `show_nav` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示到导航栏 1:显示到导航栏 0：不显示到导航栏',
  `allow_son` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1：是 0：否 是否允许添加子分类',
  `sort` smallint(6) NOT NULL DEFAULT '50' COMMENT '排序',
  `pid` smallint(6) NOT NULL DEFAULT '0' COMMENT '上级栏目id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tp_cate
-- ----------------------------
INSERT INTO `tp_cate` VALUES ('1', '系统', '1', '系统', '系统', '0', '0', '7', '0');
INSERT INTO `tp_cate` VALUES ('2', '网店帮助分类', '2', '网店帮助分类1', '网店帮助分类1', '0', '1', '11', '1');
INSERT INTO `tp_cate` VALUES ('3', '网店信息', '4', '网店信息', '网店信息', '0', '0', '12', '1');
INSERT INTO `tp_cate` VALUES ('4', '新手上路 ', '3', '新手上路 ', '新手上路 ', '0', '0', '55', '2');
INSERT INTO `tp_cate` VALUES ('5', '配送与支付 ', '3', '配送与支付 ', '配送与支付 ', '0', '0', '54', '2');
INSERT INTO `tp_cate` VALUES ('7', '会员中心', '3', '会员中心', '会员中心', '0', '0', '53', '2');
INSERT INTO `tp_cate` VALUES ('16', '服务保证', '3', '', '', '0', '1', '52', '2');
INSERT INTO `tp_cate` VALUES ('17', '联系我们', '3', '', '', '0', '1', '51', '2');
INSERT INTO `tp_cate` VALUES ('18', '3G资讯', '5', '', '', '0', '1', '6', '0');
INSERT INTO `tp_cate` VALUES ('19', '站内快讯', '5', '', '', '0', '1', '5', '0');
INSERT INTO `tp_cate` VALUES ('20', '商城公告', '5', '', '', '0', '1', '4', '0');
INSERT INTO `tp_cate` VALUES ('21', '发票问题', '5', '', '', '0', '1', '3', '0');
INSERT INTO `tp_cate` VALUES ('22', '微分销', '5', '', '', '0', '1', '1', '0');
INSERT INTO `tp_cate` VALUES ('23', 'App下载', '5', '', '', '0', '1', '2', '0');
INSERT INTO `tp_cate` VALUES ('24', 'IOS版', '5', '', '', '0', '1', '50', '23');
INSERT INTO `tp_cate` VALUES ('25', '安卓版', '5', '', '', '0', '1', '50', '23');
INSERT INTO `tp_cate` VALUES ('26', '促销信息', '5', '', '', '0', '1', '50', '0');
